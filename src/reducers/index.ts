import { combineReducers } from 'redux';

import auth from './auth';
import pageStatus from './pageStatus';
import candidates from './candidates';
import timings from './timings';
import standings from './standings';
import popups from './popup';
import prompt from './prompt';

const reducerList = {
  auth,
  pageStatus,
  candidates,
  timings,
  standings,
  popups,
  prompt,
};

export default combineReducers(reducerList);
