import { GET_TIMINGS, SET_TIMINGS } from 'actions/types';
import { Timing } from 'types/types';

const initialState: Timing[] = [];

export default (
  state = initialState,
  { type, payload }: { type: string; payload: any },
): Timing[] => {
  switch (type) {
    case GET_TIMINGS:
    case SET_TIMINGS:
      return payload;
    default:
      return state;
  }
};
