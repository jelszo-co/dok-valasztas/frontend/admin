import { GET_CANDIDATES, SET_CANDIDATES, SET_SELECTED_CANDIDATE } from 'actions/types';
import { Candidates } from 'types/stateTypes';

const initialSate: Candidates = {
  candidateList: [],
  selected: -1,
};

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): Candidates => {
  switch (type) {
    case GET_CANDIDATES:
    case SET_CANDIDATES:
      return { ...state, candidateList: payload };
    case SET_SELECTED_CANDIDATE:
      return { ...state, selected: payload };
    default:
      return state;
  }
};
