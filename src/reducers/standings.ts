import { GET_STANDINGS, SET_STANDINGS_INTERVAL } from '../actions/types';
import { GroupBy, Standings } from '../types/stateTypes';

const initialState: Standings = {
  groupBy: GroupBy.DAY,
  data: [],
};

export default (state = initialState, { type, payload }: { type: string; payload: any }) => {
  switch (type) {
    case GET_STANDINGS:
      return { ...state, data: payload };
    case SET_STANDINGS_INTERVAL:
      return { ...state, groupBy: payload };
    default:
      return state;
  }
};
