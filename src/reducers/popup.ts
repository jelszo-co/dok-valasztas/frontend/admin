import { SET_POPUP, DEL_POPUP } from '../actions/types';
import { Popup } from 'types/types';

const initialState: Popup[] = [];

export default (
  state = initialState,
  { type, payload }: { type: string; payload: any },
): Popup[] => {
  switch (type) {
    case SET_POPUP:
      return [...state, payload];
    case DEL_POPUP:
      return [...state.filter(p => p.id !== payload)];
    default:
      return state;
  }
};
