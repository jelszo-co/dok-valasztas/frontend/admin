import { Prompt, PromptStatus } from 'types/stateTypes';
import { DEL_PROMPT, SET_PROMPT_RESULT, SHOW_PROMPT } from 'actions/types';

const initialState: Prompt = {
  active: false,
  question: '',
  buttons: ['', ''],
  status: PromptStatus.Pending,
  onAccept: () => {},
  onReject: () => {},
};

export default (state = initialState, { type, payload }: { type: string; payload: any }) => {
  switch (type) {
    case SHOW_PROMPT:
      return payload;
    case SET_PROMPT_RESULT:
      return { ...state, status: payload };
    case DEL_PROMPT:
      return initialState;
    default:
      return state;
  }
};
