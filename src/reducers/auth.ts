import { AUTH_FAIL, AUTH_SUCCESS } from '../actions/types';

const initialState = {
  isLoggedIn: false,
};

export default (state = initialState, { type }: { type: string }) => {
  switch (type) {
    case AUTH_FAIL:
      return { isLoggedIn: false };
    case AUTH_SUCCESS:
      return { isLoggedIn: true };
    default:
      return state;
  }
};
