export const addZero = (num: number) => {
  if (num >= 10) {
    return num.toString();
  } else {
    return `0${num}`;
  }
};

export const formatDate = (dateInput: string | Date) => {
  const d = typeof dateInput === 'string' ? new Date(dateInput) : dateInput;

  return `${d.getFullYear()}. ${addZero(d.getMonth() + 1)}. ${addZero(d.getDate())}. ${addZero(
    d.getHours(),
  )}:${addZero(d.getMinutes())}`;
};
