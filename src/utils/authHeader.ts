import axios from 'axios';

export const setHeader = (token?: string) => {
  if (token) {
    axios.defaults.headers.common['Authorization'] = token;
    localStorage.setItem('AUTH_TOKEN', token);
    console.log('Stored token in localStorage.');
  } else {
    delete axios.defaults.headers.common['Authorization'];
    localStorage.removeItem('AUTH_TOKEN');
    console.log('Removed token from localStorage.');
  }
};

export const setHeaderFromLocalStorage = () => {
  const token = localStorage.getItem('AUTH_TOKEN');
  if (token) {
    axios.defaults.headers.common['Authorization'] = token;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
};

// export const getHeaderFromLocalStorage = () => localStorage.getItem('AUTH_TOKEN');
