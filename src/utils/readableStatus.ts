import { PageStatus } from '../types/types';

export const getReadablePageStatus = (status: PageStatus): string => {
  switch (status) {
    case PageStatus.Closed:
      return 'Oldal lekapcsolva';
    case PageStatus.Open:
      return 'Oldal elérhető';
    case PageStatus.Vote:
      return 'Szavazás elérhető';
    case PageStatus.VoteExpired:
      return 'Szavazás lezárult';
    case PageStatus.Result:
      return 'Eredmények közzétéve';
  }
};
