import { PageStatus } from './types';

export interface API_auth_login {
  token: string;
}

export interface API_status_current {
  current: PageStatus;
}
