import { Candidate, PageStatus, Popup, Timing, VoteStat } from './types';

export interface State {
  auth: Auth;
  pageStatus: PageStatus;
  candidates: Candidates;
  timings: Timing[];
  standings: Standings;
  popups: Popup[];
  prompt: Prompt;
}

export interface Auth {
  isLoggedIn: boolean;
}

export interface Candidates {
  candidateList: Candidate[];

  // -2: new
  // -1: null
  // 0-x: candidate
  selected: number;
}

export interface Prompt {
  active: boolean;
  question: string;
  buttons: [string, string];
  status: PromptStatus;
  onAccept: Function;
  onReject: Function;
}

export enum PromptStatus {
  Pending = 'PENDING',
  Accepted = 'ACCEPTED',
  Rejected = 'REJECTED',
}

export interface Standings {
  groupBy: GroupBy;
  data: VoteStat[];
}

export interface Group {
  title: number;
  stats: VoteStat[];
}

export enum GroupBy {
  MINUTE = 60000,
  HOUR = 3600000,
  HOUR12 = 4.32e7,
  DAY = 8.64e7,
}
