export enum PageStatus {
  Closed = 'CLOSED',
  Open = 'OPEN',
  Vote = 'VOTE',
  VoteExpired = 'VOTE_EXPIRED',
  Result = 'RESULT',
}
export interface Popup {
  id: string;
  msg: string;
  type: PopupType;
  expireIn?: number;
}

export enum PopupType {
  Info,
  Alert,
  Error,
}

export interface Timing {
  start: string;
  status: PageStatus;
  until: string;
}

export interface Candidate {
  id: number;
  name: string[];
  essay: string;
  description?: string;
  picture?: string;
}

export interface VoteStat {
  candidate: number;
  ts: string;
}
