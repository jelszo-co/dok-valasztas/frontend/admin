interface IconProps {
  icon: string;
  [propName: string]: any;
}

const IonIcon = (props: IconProps) => {
  let icon = props.icon.replace('data:image/svg+xml;utf8,', '');
  return <div {...props} dangerouslySetInnerHTML={{ __html: icon }} />;
};

export default IonIcon;
