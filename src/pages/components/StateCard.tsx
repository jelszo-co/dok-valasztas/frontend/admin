import { PageStatus, Timing } from 'types/types';
import { useState } from 'react';
import axios from 'axios';
import classNames from 'classnames';
import IonIcon from './IonIcon';
import { checkmarkCircleOutline, closeOutline } from 'ionicons/icons';
import CreateIcon from '@material-ui/icons/Create';
import DateTimePicker from 'react-datetime-picker';
import { formatDate } from 'utils/formatDate';
import { Colors, GeneralCTA } from '@jelszo-co/dok-components';
import { State } from 'types/stateTypes';
import { connect, ConnectedProps } from 'react-redux';
import { getTimings, setTimings } from 'actions/timings';
import { setPopupWithObject } from 'actions/popup';
import { errPopupServer } from 'templates/errPopup';
import { getReadablePageStatus } from 'utils/readableStatus';
import { getPageStatus, setPageStatus } from 'actions/pageStatus';
import { API_status_current } from 'types/apiTypes';

const mapStateToProps = (state: State) => ({
  pageStatus: state.pageStatus,
  timings: state.timings,
});

const connector = connect(mapStateToProps, {
  setTimings,
  setPageStatus,
  getTimings,
  getPageStatus,
});

interface StateCardProps extends ConnectedProps<typeof connector> {
  statusFor: PageStatus;
  showUntil?: boolean;
  minified?: boolean;
}

const StateCard = ({
  statusFor,
  timings,
  minified = false,
  showUntil = false,
  pageStatus,
  setTimings,
  setPageStatus,
  getTimings,
  getPageStatus,
}: StateCardProps) => {
  const timing = timings.find(t => t.status === statusFor);

  const [editTimes, setEditTimes] = useState(false);
  const [newStart, setNewStart] = useState(timing?.start);
  const [newUntil, setNewUntil] = useState(timing?.until);

  const delTiming = async () => {
    try {
      const { data }: { data: Timing[] } = await axios.delete(
        `${process.env.REACT_APP_SRV_ADDR}/status/timing`,
        {
          data: { current: statusFor },
        },
      );
      setTimings(data);
      getPageStatus();
    } catch (e) {
      setPopupWithObject(errPopupServer);
    }
  };

  const setCurrentState = async () => {
    try {
      const {
        data,
      }: {
        data: API_status_current;
      } = await axios.post(`${process.env.REACT_APP_SRV_ADDR}/status/current`, {
        current: statusFor,
      });
      setPageStatus(data.current);
      getTimings();
    } catch (e) {
      setPopupWithObject(errPopupServer);
    }
  };

  const handleStartEditClick = async () => {
    if (editTimes) {
      try {
        if (!newStart) return setEditTimes(false);
        const payload: Timing = {
          status: statusFor,
          start: new Date(newStart).toISOString(),
          until: newUntil
            ? new Date(newUntil).toISOString()
            : new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString(),
        };
        const { data }: { data: Timing[] } = await axios.post(
          `${process.env.REACT_APP_SRV_ADDR}/status/timing`,
          payload,
        );
        setTimings(data);

        setEditTimes(false);
      } catch (e) {
        console.error(e);
        setPopupWithObject(errPopupServer);
      }
    } else {
      setEditTimes(true);
    }
  };

  return (
    <div
      className={classNames('state-card', {
        'state-card-minified': minified,
        'state-card-active': pageStatus === statusFor,
      })}>
      <h3>{getReadablePageStatus(statusFor)}</h3>
      <div className='timing-group'>
        <h4>
          {showUntil && 'Nyitó '}időzítés{' '}
          <button onClick={() => handleStartEditClick()}>
            {editTimes ? <IonIcon icon={checkmarkCircleOutline} /> : <CreateIcon />}
          </button>
        </h4>
        {editTimes ? (
          <DateTimePicker
            onChange={(value: any) => setNewStart(value)}
            value={newStart}
            format={'y/MM/dd HH:mm'}
            minDetail={'month'}
            maxDetail={'minute'}
            minDate={new Date(new Date().getFullYear(), 0, 1)}
            maxDate={new Date(new Date().getFullYear() + 1, 0, 1)}
            clearIcon={null}
            disableClock
          />
        ) : timing ? (
          <p className='chosen-date'>
            {formatDate(timing.start)}{' '}
            <IonIcon
              icon={closeOutline}
              onClick={() => delTiming()}
              className='ionicon-close-outline'
            />
          </p>
        ) : (
          <p>Nincs</p>
        )}
      </div>
      {showUntil && (
        <div className='timing-group'>
          <h4>Záró időzítés</h4>
          {editTimes ? (
            <DateTimePicker
              onChange={(value: any) => setNewUntil(value)}
              value={newUntil}
              format={'y/MM/dd HH:mm'}
              minDetail={'month'}
              maxDetail={'minute'}
              clearIcon={null}
              disableClock
            />
          ) : timing ? (
            <p className='chosen-date'>
              {formatDate(timing.until)}{' '}
              <IonIcon
                icon={closeOutline}
                onClick={() => delTiming()}
                className='ionicon-close-outline'
              />
            </p>
          ) : (
            <p>Nincs</p>
          )}
        </div>
      )}

      <div className='state-switcher'>
        {pageStatus === statusFor ? (
          <h6>{!minified && 'Állapot '}aktív</h6>
        ) : (
          <GeneralCTA
            color={Colors.Bright}
            bgColor={Colors.Gradient}
            callback={() => setCurrentState()}>
            {minified ? 'Aktiválás' : 'Állapot aktiválása'}
          </GeneralCTA>
        )}
      </div>
    </div>
  );
};

export default connector(StateCard);
