import { Popup } from 'types/types';
import { State } from 'types/stateTypes';
import { connect } from 'react-redux';
import { delPopup } from 'actions/popup';
import { warning, alertCircle, informationCircle } from 'ionicons/icons';
import IonIcon from './IonIcon';

import 'scss/components/Popup.scss';

const PopupWrapper = ({ popups, delPopup }: { popups: Popup[]; delPopup: any }) => {
  return (
    <div id='popup-wrapper'>
      {popups.map(popup => (
        <PopupComponent popup={popup} delPopup={delPopup} key={popup.id} />
      ))}
    </div>
  );
};

const icons = [informationCircle, warning, alertCircle];

const PopupComponent = ({ popup, delPopup }: { popup: Popup; delPopup: any }) => {
  if (popup.expireIn)
    setTimeout(() => {
      delPopup(popup.id);
    }, popup.expireIn * 1000);

  console.log(popup);

  return (
    <div id={'p' + popup.id} className='popup'>
      <div className='popup-content'>
        <IonIcon icon={icons[popup.type]} className={`popup-icon popup-icon-${popup.type}`} />
        <p>{popup.msg}</p>
      </div>
      {!popup.expireIn && <button onClick={() => delPopup(popup.id)}>Ok</button>}
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  popups: state.popups,
});

export default connect(mapStateToProps, { delPopup })(PopupWrapper);
