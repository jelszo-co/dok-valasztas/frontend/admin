import React, { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';
import { PromptStatus, State } from 'types/stateTypes';
import { connect, ConnectedProps } from 'react-redux';
import { Candidate, PopupType } from 'types/types';
import { Colors, GeneralCTA } from '@jelszo-co/dok-components';
import IonIcon from './IonIcon';
import {
  checkmark,
  closeOutline,
  documentAttachOutline,
  syncOutline,
  trashOutline,
} from 'ionicons/icons';
import { useDropzone } from 'react-dropzone';
import isEmpty from 'lodash/isEmpty';
import { setPopup } from 'actions/popup';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { setSelectedCandidate } from '../../actions/candidates';
import { delPrompt, showPrompt } from '../../actions/prompt';

import 'scss/components/CandidateEditor.scss';
import axios from 'axios';

const mapStateToProps = (state: State) => ({
  selected: state.candidates.selected,
  candidates: state.candidates.candidateList,
  promptStatus: state.prompt.status,
});

const connector = connect(mapStateToProps, {
  setPopup,
  setSelectedCandidate,
  showPrompt,
  delPrompt,
});

const Editor = ({
  selected,
  candidates,
  setPopup,
  setSelectedCandidate,
  showPrompt,
  delPrompt,
}: ConnectedProps<typeof connector>) => {
  // ****************
  // BASIC SETUP
  // ****************

  if (selected === -1) return <></>;

  useEffect(() => {
    setTimeout(() => {
      window.scrollTo({ top: 100000000, left: 0, behavior: 'smooth' });
    }, 500);
  }, []);

  const [SC, setSC] = useState<Partial<Candidate>>({});
  const [originalSC, setOriginalSC] = useState<Partial<Candidate>>({});

  useEffect(() => {
    const found = candidates.find(c => c.id === selected);

    if (found) {
      axios.get(`${process.env.REACT_APP_SRV_ADDR}/candidate/essay/${found.id}`).then(res => {
        setSC({ ...found, essay: res.data.essay });
        setOriginalSC({ ...found, essay: res.data.essay });
      });
    } else {
      setSC({});
      setOriginalSC({});
    }
  }, [selected]);

  const saveCandidate = () => {
    // Make API request
    setSelectedCandidate(-1);
  };

  const delCandidate = async () => {
    const status = await showPrompt('Biztosan törlöd a jelöltet?', ['mégse', 'igen']);
    if (status === PromptStatus.Accepted) {
      // Make API request
      delPrompt();
    } else {
      delPrompt();
    }
  };

  const handleClose = async () => {
    if (JSON.stringify(SC) === JSON.stringify(originalSC)) {
      return setSelectedCandidate(-1);
    } else {
      const status = await showPrompt('Nem mentettél. Biztosan bezárod?', ['vissza', 'igen']);
      if (status === PromptStatus.Accepted) setSelectedCandidate(-1);
      delPrompt();
    }
  };

  // ****************
  // NAMES
  // ****************

  const [addName, setAddName] = useState(false);
  const [newName, setNewName] = useState('');

  const handleNewName = () => {
    if (newName.length === 0) return;
    const newNames: string[] = SC.name ? SC.name.concat([newName]) : [newName];
    setSC({ ...SC, name: newNames });
    setAddName(false);
    setNewName('');
  };

  // https://codesandbox.io/s/k260nyxq9v?file=/index.js
  const reorder = (list: string[], startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return;
    if (!SC.name) return;

    const names = reorder(SC.name, result.source.index, result.destination.index);

    setSC({ ...SC, name: names });
  };

  // ****************
  // ESSAY
  // ****************

  const [editEssay, setEditEssay] = useState(true);
  const [filename, setFilename] = useState('');

  useEffect(() => {
    if (SC && SC.essay) setEditEssay(false);
  }, [SC]);

  const toBase64 = (file: File) =>
    new Promise<string>(resolve => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (!reader.result) return setPopup(PopupType.Error, 'Érvénytelen fájl.');
        if (typeof reader.result !== 'string') return;
        resolve(reader.result);
      };
    });

  const onDrop = useCallback(async files => {
    console.log(files);
    if (files.length > 1) return setPopup(PopupType.Error, 'Csak egy fájlt tölts fel.', 5);
    const file = files[0];
    const b64file = await toBase64(file);
    setSC({ ...SC, essay: b64file });
    setFilename(file.name);
    setEditEssay(false);
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: 'application/pdf',
  });

  const renderEssayEditor = () => {
    if (isEmpty(SC.essay) || editEssay)
      return (
        <div {...getRootProps()} className='dragndrop-container'>
          <input {...getInputProps()} />
          {isDragActive ? (
            <p>Húzd ide a fájlt...</p>
          ) : (
            <p>Húzd ide a fájlt, vagy kattints a kiválasztáshoz</p>
          )}
        </div>
      );
    return (
      <div className='essay-editor-container'>
        <IonIcon icon={documentAttachOutline} /> <p>{filename || 'palyazat.pdf'}</p>{' '}
        <IonIcon icon={syncOutline} onClick={() => setEditEssay(true)} />
      </div>
    );
  };

  // ****************
  // PICTURE
  // ****************

  const pictureInput = useRef<HTMLInputElement>(null);

  const handleNewPicture = async (e: ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) return;
    if (e.target.files.length > 1) return setPopup(PopupType.Error, 'Csak egy fájlt tölts fel.', 5);
    const file = e.target.files[0];
    const b64file = await toBase64(file);
    setSC({ ...SC, picture: b64file.replace('data:image/png;base64,', '') });
  };

  const delPhoto = () => {
    setSC({ ...SC, picture: '' });
  };

  return (
    <div id={'editor'}>
      <IonIcon icon={closeOutline} className={'editor-close'} onClick={() => handleClose()} />
      <div className='editor-title-button'>
        <h2>Szerkesztés</h2>
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => saveCandidate()}>
          Mentés
        </GeneralCTA>
      </div>
      <div className='editor-col-container'>
        <div className='editor-col' id={'editor-col-1'}>
          <h3>Nevek</h3>
          {/* Drag'n'drop goes BRRR */}
          {SC.name && SC.name.length > 0 && (
            <DragDropContext onDragEnd={result => onDragEnd(result)}>
              <Droppable droppableId={'droppable'}>
                {provided => (
                  <div {...provided.droppableProps} ref={provided.innerRef} className='editor-name'>
                    {SC.name?.map((name, index) => (
                      <Draggable key={name} draggableId={`draggable-${name}`} index={index}>
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            style={{ ...provided.draggableProps.style }}>
                            <span {...provided.dragHandleProps}>⣿</span> {name}
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          )}
          <div className='editor-add-name'>
            {addName ? (
              <div className={'input-group'}>
                <input
                  autoFocus
                  type='text'
                  placeholder={'Új név'}
                  value={newName}
                  onChange={e => setNewName(e.target.value)}
                  onKeyDown={e => {
                    e.key === 'Enter' && handleNewName();
                    e.key === 'Escape' && setAddName(false);
                  }}
                />
                <IonIcon icon={checkmark} onClick={() => handleNewName()} />
              </div>
            ) : (
              <button className={'add-button'} onClick={() => setAddName(true)}>
                Hozzáadás
              </button>
            )}
          </div>
          <h3>Bemutatkozás</h3>
          <div className='textarea-container'>
            <textarea
              rows={10}
              value={SC.description || ''}
              onChange={e => setSC({ ...SC, description: e.target.value })}
            />
          </div>
        </div>
        <div className='editor-col' id={'editor-col-2'}>
          <h3>Pályázat</h3>
          {renderEssayEditor()}
          <h3>Fénykép</h3>
          <div className='editor-photo-actions'>
            <button onClick={() => pictureInput.current && pictureInput.current.click()}>
              {isEmpty(SC.picture) ? 'Kiválasztás' : 'Csere'}
            </button>
            <input
              type={'file'}
              ref={pictureInput}
              accept={'image/*'}
              multiple={false}
              onChange={e => handleNewPicture(e)}
            />
            {!isEmpty(SC.picture) && (
              <>
                <p>|</p>
                <button onClick={() => delPhoto()}>Törlés</button>
                <div className='flex-break' />
                <img src={`data:image/png;base64,${SC.picture}`} alt='Kép' />
              </>
            )}
          </div>
        </div>
      </div>
      <div className='del-candidate'>
        <button onClick={() => (selected === -2 ? handleClose() : delCandidate())}>
          <IonIcon icon={trashOutline} />
        </button>
      </div>
    </div>
  );
};

export default connector(Editor);
