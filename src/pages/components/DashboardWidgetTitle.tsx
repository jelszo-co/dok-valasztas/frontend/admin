import { FunctionComponent } from 'react';
import 'scss/components/DashboardWidgetTitle.scss';

export const DashboardWidgetTitle: FunctionComponent = ({ children }) => (
  <h2 className={'dashboard-widget-title'}>{children}</h2>
);
