import { connect, ConnectedProps } from 'react-redux';
import { PromptStatus, State } from 'types/stateTypes';
import { setPromptResult } from '../../actions/prompt';

import 'scss/components/Prompt.scss';

const mapStateToProps = (state: State) => ({
  prompt: state.prompt,
});

const connector = connect(mapStateToProps);

const Prompt = ({ prompt }: ConnectedProps<typeof connector>) => {
  if (!prompt.active) return <></>;
  return (
    <div id={'prompt-wrapper'}>
      <div id='prompt'>
        <p>{prompt.question}</p>
        <button onClick={() => prompt.onReject()} className='cancel'>
          {prompt.buttons[0]}
        </button>
        <button onClick={() => prompt.onAccept()} className='ok'>
          {prompt.buttons[1]}
        </button>
      </div>
    </div>
  );
};

export default connector(Prompt);
