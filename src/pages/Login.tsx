import { FormEvent, useState } from 'react';
import IonIcon from './components/IonIcon';
import { chevronForwardCircle } from 'ionicons/icons';
import { Redirect } from 'react-router-dom';

import 'scss/Login.scss';
import { connect, ConnectedProps } from 'react-redux';
import { loginUser } from '../actions/auth';
import { Auth, State } from '../types/stateTypes';

const mapStateToProps = (state: State) => ({
  auth: state.auth,
});

const connector = connect(mapStateToProps, { loginUser });

const Login = ({ auth, loginUser }: ConnectedProps<typeof connector>) => {
  const [pass, setPass] = useState('');

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    loginUser(pass);
  };

  if (auth.isLoggedIn) return <Redirect to={'/'} />;
  return (
    <div id={'login'}>
      <h1>Dök elnök választás</h1>
      <h3>Vezérlőpult</h3>

      <form className='login-form' onSubmit={e => handleSubmit(e)}>
        <div className='input-wrapper'>
          <input
            type='password'
            placeholder={'Jelszó'}
            value={pass}
            onChange={e => setPass(e.target.value)}
          />
        </div>
        <button type='submit'>
          <IonIcon icon={chevronForwardCircle} />
        </button>
      </form>
    </div>
  );
};

export default connector(Login);
