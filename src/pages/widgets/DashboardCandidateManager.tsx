import { DashboardWidgetTitle } from '../components/DashboardWidgetTitle';
import { Colors, GeneralCTA, Image } from '@jelszo-co/dok-components';
import { State } from '../../types/stateTypes';
import { connect, ConnectedProps } from 'react-redux';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import { setSelectedCandidate } from 'actions/candidates';
import Editor from '../components/CandidateEditor';
import IonIcon from '../components/IonIcon';
import { addOutline } from 'ionicons/icons';

import 'scss/widgets/DashboardCandidateManager.scss';

const mapStateToProps = (state: State) => ({
  candidates: state.candidates.candidateList,
});

const connector = connect(mapStateToProps, { setSelectedCandidate });

const DashboardCandidateManager = ({
  candidates,
  setSelectedCandidate,
}: ConnectedProps<typeof connector>) => {
  return (
    <section id={'dashboard-candidate-manager'}>
      <div className='title-bar'>
        <DashboardWidgetTitle>Jelöltek</DashboardWidgetTitle>
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => setSelectedCandidate(-2)}>
          <IonIcon icon={addOutline} /> Hozzáadás
        </GeneralCTA>
      </div>
      <div className='candidates'>
        {candidates.map(c => (
          <div
            className={classNames('editor-card', {
              'editor-card-image': !isEmpty(c.picture),
            })}
            key={c.id}>
            {!isEmpty(c.picture) && <Image base64Image={c.picture ? c.picture : ''} uid={c.id} />}
            {c.name.map(name => (
              <h3 className='candidate-name' key={name}>
                {name}
              </h3>
            ))}
            <p>{c.description}</p>
            <button type={'button'} onClick={() => setSelectedCandidate(c.id)}>
              Szerkesztés
            </button>
          </div>
        ))}
      </div>
      <Editor />
    </section>
  );
};

export default connector(DashboardCandidateManager);
