import { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Chart from 'chart.js';
import { DashboardWidgetTitle } from '../components/DashboardWidgetTitle';
import { getStandings, setStandingsInterval } from 'actions/standings';
import { Group, GroupBy, State } from 'types/stateTypes';
import { PageStatus, Timing, VoteStat } from 'types/types';
import find from 'lodash/find';

import 'scss/widgets/DashboardStandingsGraph.scss';
import { addZero } from 'utils/formatDate';
import classnames from 'classnames';
import isMobile from 'utils/isMobile';

const mapStateToProps = (state: State) => ({
  standings: state.standings,
  candidates: state.candidates.candidateList,
  timings: state.timings,
});

const connector = connect(mapStateToProps, { getStandings, setStandingsInterval });

const groupBy = (arr: VoteStat[], by: GroupBy): Group[] => {
  let grouped: Group[] = [];
  for (let i = 0; i < arr.length; i++) {
    const d = new Date(arr[i].ts);
    const groupTitle = new Date(Math.floor(d.getTime() / by) * by).getTime();
    const current = find(grouped, { title: groupTitle });
    if (current) {
      current.stats.push(arr[i]);
    } else {
      grouped.push({ title: groupTitle, stats: [arr[i]] });
    }
  }
  return grouped;
};

const DashboardStandingsGraph = ({
  getStandings,
  setStandingsInterval,
  standings,
  candidates,
  timings,
}: ConnectedProps<typeof connector>) => {
  useEffect(() => {
    getStandings();
  }, [standings.groupBy]);

  const colorsBrightDim: string[] = [
    'rgba(239, 35, 60, 0.7)',
    'rgba(255, 130, 8, 0.7)',
    'rgba(122, 199, 79, 0.7)',
    'rgba(123,21,142,0.7)',
  ];

  const colorsBrightDimMore: string[] = [
    'rgba(239, 35, 60, 0.1)',
    'rgba(255, 130, 8, 0.1)',
    'rgba(122, 199, 79, 0.1)',
    'rgba(123, 21, 142, 0.1)',
  ];

  const colorsBright: string[] = ['#ef233c', '#ff8208', '#7ac74f', '#7B158E'];

  const [currentTiming, setCurrentTiming] = useState<Timing | undefined>(undefined);

  let standingsChart: Chart | null = null;

  const showFullTime = JSON.parse(localStorage.getItem('showFullTime') || 'true');

  useEffect(() => {
    const canvas = document.getElementById('chart');
    // @ts-ignore
    const ctx = canvas.getContext('2d');

    const getLinearGradient = (c1: string, c2: string) => {
      const gradient = ctx.createLinearGradient(0, 0, 0, 650);
      gradient.addColorStop(0, c1);
      gradient.addColorStop(1, c2);
      return gradient;
    };

    const getUnit = () => {
      switch (standings.groupBy) {
        case GroupBy.HOUR:
          return 'hour';
        case GroupBy.MINUTE:
          return 'minute';
        default:
          return 'day';
      }
    };

    standingsChart = new Chart(ctx, {
      type: 'line',
      data: {
        datasets: candidates.map((c, i) => {
          let currentCount = 0;
          return {
            label: c.name.join(', '),
            data: groupBy(
              standings.data.filter(s => s.candidate === c.id),
              standings.groupBy,
            ).map(g => {
              currentCount += g.stats.length;
              return {
                x: g.title,
                y: currentCount,
              };
            }),
            backgroundColor: getLinearGradient(colorsBrightDim[i % 4], colorsBrightDimMore[i % 4]),
            borderColor: colorsBright[i % 4],
          };
        }),
      },
      options: {
        aspectRatio: isMobile() ? 1 : 2,

        animation: {
          easing: 'easeInOutCubic',
        },
        tooltips: {
          xPadding: 20,
          yPadding: 10,
          intersect: false,
          displayColors: false,
          backgroundColor: '#1c1823',
          titleFontFamily: 'FuturaND-DemiboldOblique',
          titleFontStyle: 'italic',
          titleFontSize: 14,
          bodyFontFamily: 'FuturaND',
          callbacks: {
            title: (item, data) => {
              const d = new Date(String(item[0].xLabel));
              let displayString = `${addZero(d.getMonth() + 1)}.${addZero(d.getDate())}.`;
              if (standings.groupBy <= GroupBy.HOUR)
                displayString += ` ${addZero(d.getHours())}:${
                  standings.groupBy === GroupBy.MINUTE ? addZero(d.getMinutes()) : '00'
                }`;
              return displayString;
            },
          },
        },
        legend: {
          display: false,
          labels: {
            fontFamily: 'FuturaND',
            fontSize: 16,
            fontStyle: 'italic',
          },
        },
        legendCallback(chart: Chart): string {
          if (!chart.data.datasets) return '';
          if (chart.data.datasets.length < candidates.length) return '';

          return chart.data.datasets
            .map((ds, i) => {
              return `
                <div class="wrapper">
                <span style="background-color: ${colorsBright[i % 4]}"></span>
                <p>${ds.label}: ${
                // @ts-ignore
                ds.data && ds.data[ds.data.length - 1]?.y
              }</p>
                </div>`;
            })
            .join('');
        },
        scales: {
          xAxes: [
            {
              type: 'time',
              ticks: {
                fontFamily: 'FuturaND',
                fontSize: isMobile() ? 12 : 16,
                min: showFullTime && currentTiming ? currentTiming.start : undefined,
                max: showFullTime && currentTiming ? currentTiming.until : undefined,
              },
              time: {
                isoWeekday: true,
                unit: getUnit(),
                displayFormats: {
                  day: 'MMM D',
                  hour: 'HH',
                  minute: 'HH:mm',
                },
              },
              gridLines: {
                zeroLineColor: 'rgba(0,0,0,0.1)',
              },
            },
          ],
          yAxes: [
            {
              ticks: { fontFamily: 'FuturaND', fontSize: isMobile() ? 12 : 18 },
            },
          ],
        },
      },
    });

    console.log(standingsChart);

    // @ts-ignore
    document.querySelector('.chart-legend').innerHTML = standingsChart.generateLegend();

    return () => {
      standingsChart && standingsChart.destroy();
    };
  }, [standings.data]);

  useEffect(() => {
    setCurrentTiming(timings.find(t => t.status === PageStatus.Vote));
    console.log('Current timing set, updating chart', standingsChart);
    standingsChart && standingsChart.update();
  }, [timings]);

  return (
    <section id={'dashboard-standings-graph'}>
      <DashboardWidgetTitle>Jelenlegi állás</DashboardWidgetTitle>
      <div className='chart-wrapper'>
        <div className='chart-actions'>
          {currentTiming && (
            <button
              className={classnames('time-switcher', { active: showFullTime })}
              onClick={() => {
                localStorage.setItem('showFullTime', JSON.stringify(!showFullTime));
                window.location.reload();
              }}>
              Teljes idő
            </button>
          )}
          <div className='interval-switcher'>
            <span />
            <button
              className={classnames({ active: standings.groupBy === GroupBy.MINUTE })}
              onClick={() => setStandingsInterval(GroupBy.MINUTE)}>
              1m
            </button>
            <button
              className={classnames({ active: standings.groupBy === GroupBy.HOUR })}
              onClick={() => setStandingsInterval(GroupBy.HOUR)}>
              1h
            </button>
            <button
              className={classnames({ active: standings.groupBy === GroupBy.HOUR12 })}
              onClick={() => setStandingsInterval(GroupBy.HOUR12)}>
              12h
            </button>
            <button
              className={classnames({ active: standings.groupBy === GroupBy.DAY })}
              onClick={() => setStandingsInterval(GroupBy.DAY)}>
              1d
            </button>
          </div>
        </div>
        <div className='chart-legend' />
        <canvas id='chart' />
      </div>
    </section>
  );
};

export default connector(DashboardStandingsGraph);
