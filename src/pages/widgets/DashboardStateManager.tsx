import { PageStatus } from 'types/types';
import { DashboardWidgetTitle } from '../components/DashboardWidgetTitle';
import StateCard from '../components/StateCard';

import 'scss/widgets/DashboardStateManager.scss';
import { Colors, GeneralCTA } from '@jelszo-co/dok-components';
import { API_status_current } from 'types/apiTypes';
import axios from 'axios';
import { setPopupWithObject } from 'actions/popup';
import { errPopupServer } from 'templates/errPopup';
import { connect, ConnectedProps } from 'react-redux';
import { setPageStatus } from 'actions/pageStatus';
import { State } from 'types/stateTypes';
import { getReadablePageStatus } from '../../utils/readableStatus';

const mapStateToProps = (state: State) => ({
  status: state.pageStatus,
});

const connector = connect(mapStateToProps, { setPageStatus });

const DashboardStateManager = ({ status, setPageStatus }: ConnectedProps<typeof connector>) => {
  const setClosedState = async () => {
    try {
      const {
        data,
      }: {
        data: API_status_current;
      } = await axios.post(`${process.env.REACT_APP_SRV_ADDR}/status/current`, {
        current: PageStatus.Closed,
      });
      setPageStatus(data.current);
    } catch (e) {
      setPopupWithObject(errPopupServer);
    }
  };
  return (
    <section id={'dashboard-state-manager'}>
      <div className='title-bar title-bar-flex'>
        <DashboardWidgetTitle>Állapot</DashboardWidgetTitle>
        {status === PageStatus.Closed ? (
          <h6 className='dashboard-state-disabled'>{getReadablePageStatus(PageStatus.Closed)}</h6>
        ) : (
          <GeneralCTA
            color={Colors.Bright}
            bgColor={Colors.Gradient}
            callback={() => setClosedState()}>
            Oldal lekapcsolása
          </GeneralCTA>
        )}
      </div>
      <div className='state-cards'>
        <StateCard statusFor={PageStatus.Open} />
        <div className='state-cards-middle-group'>
          <StateCard statusFor={PageStatus.Vote} showUntil />
          <StateCard statusFor={PageStatus.VoteExpired} minified />
        </div>
        <StateCard statusFor={PageStatus.Result} />
      </div>
    </section>
  );
};

export default connector(DashboardStateManager);
