import { State } from '../types/stateTypes';
import { connect, ConnectedProps } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getReadablePageStatus } from 'utils/readableStatus';
import DashboardStateManager from './widgets/DashboardStateManager';
import DashboardCandidateManager from './widgets/DashboardCandidateManager';

import 'scss/Dashboard.scss';
import { PageStatus } from '../types/types';
import DashboardStandingsGraph from './widgets/DashboardStandingsGraph';

const mapStateToProps = (state: State) => ({
  isLoggedIn: state.auth.isLoggedIn,
  pageStatus: state.pageStatus,
});

const connector = connect(mapStateToProps);

const Dashboard = ({ isLoggedIn, pageStatus }: ConnectedProps<typeof connector>) => {
  if (!isLoggedIn) return <Redirect to={'/login'} />;
  return (
    <div id={'dashboard'}>
      <div className='title-container'>
        <h1 className={'dashboard-title'}>Vezérlőpult</h1>
        <h2 className={'dashboard-subtitle'}>
          Jelenlegi állapot: <span>{getReadablePageStatus(pageStatus)}</span>
        </h2>
      </div>
      {(pageStatus === PageStatus.Vote || pageStatus === PageStatus.VoteExpired || pageStatus === PageStatus.Result) && <DashboardStandingsGraph />}
      <DashboardStateManager />
      <DashboardCandidateManager />
    </div>
  );
};

export default connector(Dashboard);
