import { Prompt, PromptStatus } from '../types/stateTypes';
import { DEL_PROMPT, SET_PROMPT_RESULT, SHOW_PROMPT } from './types';

export const showPrompt = (question: string, buttons: [string, string]) => (dispatch: any) =>
  new Promise<PromptStatus>((resolve, reject) => {
    if (!buttons || !question) return console.error('Not enough args provided!');
    const newPrompt: Prompt = {
      buttons,
      question,
      active: true,
      status: PromptStatus.Pending,
      onAccept: () => {
        dispatch(setPromptResult(PromptStatus.Accepted));
        resolve(PromptStatus.Accepted);
      },
      onReject: () => {
        dispatch(setPromptResult(PromptStatus.Rejected));
        resolve(PromptStatus.Rejected);
      },
    };
    dispatch({ type: SHOW_PROMPT, payload: newPrompt });
  });

export const delPrompt = () => (dispatch: any) => dispatch({ type: DEL_PROMPT });

export const setPromptResult = (result: PromptStatus) => (dispatch: any) =>
  dispatch({ type: SET_PROMPT_RESULT, payload: result });
