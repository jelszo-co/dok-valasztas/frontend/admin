import axios from 'axios';
import { errPopupServer } from 'templates/errPopup';
import { Candidate } from 'types/types';
import { GET_CANDIDATES, SET_SELECTED_CANDIDATE, SET_POPUP } from './types';

export const getCandidates = () => async (dispatch: any) => {
  try {
    const { data }: { data: Candidate[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/candidate`,
    );
    dispatch({
      type: GET_CANDIDATES,
      payload: data,
    });
  } catch (err) {
    dispatch({ type: SET_POPUP, payload: errPopupServer });
    console.error(err);
  }
};

export const setSelectedCandidate = (id: number) => (dispatch: any) =>
  dispatch({ type: SET_SELECTED_CANDIDATE, payload: id });
