import { v4 as uuidv4 } from 'uuid';
import { Popup, PopupType } from 'types/types';
import { DEL_POPUP, SET_POPUP } from './types';

export const setPopup = (type: PopupType, msg: string, expireIn?: number) => (dispatch: any) => {
  const payload: Popup = {
    id: uuidv4(),
    msg,
    type,
    expireIn,
  };
  dispatch({ type: SET_POPUP, payload });
};

export const setPopupWithObject = (payload: Popup) => (dispatch: any) => {
  dispatch({ type: SET_POPUP, payload });
};

export const delPopup = (id: string) => (dispatch: any) => {
  const el = document.querySelector(`#p${id}`);
  el?.classList.add('popup-fade-out');
  setTimeout(() => {
    dispatch({ type: DEL_POPUP, payload: id });
  }, 200);
};
