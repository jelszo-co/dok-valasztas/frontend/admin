import { Timing } from 'types/types';
import axios from 'axios';
import { errPopupServer } from '../templates/errPopup';
import { GET_TIMINGS, SET_TIMINGS } from './types';
import { setPopupWithObject } from './popup';

export const getTimings = () => async (dispatch: any) => {
  try {
    const { data }: { data: Timing[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/status/timing`,
    );
    return dispatch({ type: GET_TIMINGS, payload: data });
  } catch (err) {
    console.error(err);
    setPopupWithObject(errPopupServer);
  }
};

export const setTimings = (timings: Timing[]) => (dispatch: any) =>
  dispatch({ type: SET_TIMINGS, payload: timings });
