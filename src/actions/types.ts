export const GET_CANDIDATES = 'GET_CANDIDATES';
export const SET_CANDIDATES = 'SET_CANDIDATES';
export const SET_SELECTED_CANDIDATE = 'SET_CURRENT_CANDIDATE';
export const GET_TIMINGS = 'GET_TIMINGS';
export const SET_TIMINGS = 'SET_TIMINGS';
export const GET_PAGE_STATUS = 'GET_PAGE_STATUS';
export const SET_PAGE_STATUS = 'SET_PAGE_STATUS';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const SET_POPUP = 'SET_POPUP';
export const DEL_POPUP = 'DEL_POPUP';
export const SHOW_PROMPT = 'SHOW_PROMPT';
export const SET_PROMPT_RESULT = 'SET_PROMPT_RESULT';
export const DEL_PROMPT = 'DEL_PROMPT';
export const GET_STANDINGS = 'GET_STANDINGS';
export const SET_STANDINGS_INTERVAL = 'SET_STANDINGS_INTERVAL';
