import { AUTH_FAIL, AUTH_SUCCESS } from './types';
import { API_auth_login } from 'types/apiTypes';
import axios from 'axios';
import { setHeader, setHeaderFromLocalStorage } from 'utils/authHeader';

export const loginUser = (pass: string) => async (dispatch: any) => {
  try {
    const { data }: { data: API_auth_login } = await axios.post(
      `${process.env.REACT_APP_SRV_ADDR}/auth/login`,
      {
        passcode: pass,
      },
    );
    dispatch({ type: AUTH_SUCCESS });
    setHeader(data.token);
  } catch (e) {
    console.error(e);
    dispatch({ type: AUTH_FAIL });
    setHeader();
  }
};

export const renewUser = () => async (dispatch: any) => {
  try {
    setHeaderFromLocalStorage();
    const { data }: { data: API_auth_login } = await axios.post(
      `${process.env.REACT_APP_SRV_ADDR}/auth/renew`,
    );
    dispatch({ type: AUTH_SUCCESS });
    setHeader(data.token);
  } catch (e) {
    console.error(e);
    dispatch({ type: AUTH_FAIL });
    setHeader();
  }
};
