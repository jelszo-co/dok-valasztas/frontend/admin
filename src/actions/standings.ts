import axios from 'axios';
import { GET_STANDINGS, SET_STANDINGS_INTERVAL } from './types';
import { VoteStat } from 'types/types';
import { setPopupWithObject } from './popup';
import { errPopupServer } from 'templates/errPopup';
import { GroupBy } from 'types/stateTypes';

export const getStandings = () => (dispatch: any) =>
  new Promise(async resolve => {
    try {
      console.log('Getting stats...');
      const { data }: { data: VoteStat[] } = await axios.get(
        `${process.env.REACT_APP_SRV_ADDR}/vote/stats`,
      );
      dispatch({ type: GET_STANDINGS, payload: data });
      resolve(null);
    } catch (e) {
      console.error(e);
      setPopupWithObject(errPopupServer);
    }
  });

export const setStandingsInterval = (by: GroupBy) => (dispatch: any) =>
  dispatch({ type: SET_STANDINGS_INTERVAL, payload: by });
