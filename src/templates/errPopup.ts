import { v4 as uuidv4 } from 'uuid';
import { Popup, PopupType } from 'types/types';

export const errPopupServer: Popup = {
  id: uuidv4(),
  msg: 'Hiba a szerverrel való kommunikáció során.',
  type: PopupType.Error,
  expireIn: 7,
};

export const errPopupRatelimit: Popup = {
  id: uuidv4(),
  msg: 'Woah! Túl sok rossz próbálkozásod volt. Várj egy kicsit a következő előtt.',
  type: PopupType.Error,
  expireIn: 9,
};
