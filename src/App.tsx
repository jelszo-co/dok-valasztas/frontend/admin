import { useEffect } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { renewUser } from './actions/auth';
import { getPageStatus } from './actions/pageStatus';
import { getTimings } from './actions/timings';

import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import PopupWrapper from './pages/components/Popup';

import 'scss/App.scss';
import { getCandidates } from './actions/candidates';
import Prompt from './pages/components/Prompt';

const App = () => {
  useEffect(() => {
    store.dispatch(renewUser());
    store.dispatch(getPageStatus());
    store.dispatch(getCandidates());
    store.dispatch(getTimings());
  });
  return (
    <Provider store={store}>
      <PopupWrapper />
      <Prompt />
      <svg className='base-gradient-holder'>
        <defs>
          <linearGradient id='base-gradient' x1='0%' y1='0%' x2='0%' y2='100%'>
            <stop offset='0' stopColor='#ef233c' />
            <stop offset='1' stopColor='#ff8208' />
          </linearGradient>
        </defs>
      </svg>
      <Router>
        <Switch>
          <Route exact path={'/'} component={Dashboard} />
          <Route exact path={'/login'} component={Login} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
